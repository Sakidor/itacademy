import UIKit

/**
 Take a look at Coin enumeration.
 Write a function where you can pass in the array of coins,
 add up the value and then return the number of cents.
 */


enum Coin: Int {
    case penny = 1
    case nickel = 5
    case dime = 10
    case quarter = 25
}

func getSumOfCoins (Coins : [Coin]) -> Int {
    var sum = 0
    for i in Coins{
        sum += i.rawValue
    }
    return sum

}

getSumOfCoins(Coins: [.penny, .nickel, .dime, .quarter])


/**
 Take a look at Month enumeration.
 Write a computed property to calculate the number of months until summer.
 Hint: You’ll need to account for a negative value if summer has already passed in the current year. To do that, imagine looping back around for the next full year.
 */


enum Month: Int {
    case january = 1, february, march, april, may, june, july,
    august, september, october, november, december
    
    
    var calculateMonthsUntilSummer : String {
        switch self {
        case .june,.july,.august :
            return "it's summer!"
        default:
            let countOfMonths = Month.june.rawValue - self.rawValue
            if (countOfMonths < 0){
                return "\(countOfMonths + 12) months until summer"
            }
            else {
                return "\(countOfMonths) months until summer"
            }
        }
    }
}

var month = Month.december
month.calculateMonthsUntilSummer



/**
 Take a look at Direction enumeration.
 Imagine starting a new level in a video game. The character makes a series of movements in the game. Calculate the position of the character on a top-down level map after making a set of movements:
 
 let movements: [Direction] = [.north, .north, .west, .south,
 .west, .south, .south, .east, .east, .south, .east]
 
 Hint: Use a tuple for the location:
 var location = (x: 0, y: 0)
 */


enum Direction {
    case north
    case south
    case east
    case west
}



func getLocationOfCharacter (directions : [Direction]) -> (Int, Int) {
    var location = (x: 0, y: 0)
    for direction in directions {
        if direction == .north {
            location.y += 1
        }
        else if direction == .south{
            location.y -= 1
        }
        else if direction == .east{
            location.x += 1
        }
        else if direction == .west{
            location.x -= 1
        }
    }
    return location
}

let movements: [Direction] = [.north, .north, .west, .south, .west, .south, .south, .east, .east, .south, .east]

getLocationOfCharacter(directions: movements)
