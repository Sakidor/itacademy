import UIKit

/*:
 Task 1
 - Define constant 'x' with type Int
 - Define variable 'y' with type Double
 - Define Optional varibale 'sum' with type Int wihout initial value
 - Set 'sum' equal to the sum of x and y
 */

//Answer

let x : Int = 5
var y : Double = 6.13
var sum : Int?

sum = x + Int(y)


/*:
 Task 2
 Print string 'x + y = sum' into console where 'sum' replaced by value from task 1.
 Pay attention, keyword Optional() should not be printed out around the value
 */

//Answer
if let sum = sum {
    print("x + y = \(sum)")
}

/*:
 Task 3
 Define a function isOdd(), which returns Bool value and determines is 'sum' (from tasks 1 & 2) is odd.
 3. Объявите функцию isOdd(), возвращающую Bool, которая будет показывать, верно ли что число sum нечетное.
 */

//Answer
func isOdd() -> Bool {
    guard let sum = sum else {
        return false
    }
    return sum % 2 != 0
}
isOdd()


/*
 Task 4
 Write a function named printFullName that takes two strings called firstName and lastName.
 The function should print out the full name defined as firstName + " " + lastName. Use it to print out your own full name.
 */

//Answer

func printFullName(firstName : String, lastName : String){
    print(firstName + " " + lastName)
}

printFullName(firstName: "Pasha", lastName: "Polhovskiy")


/*
 Task 5
 Change the declaration of printFullName to have no external name for either parameter.
 */

//Answer

func printFullName(_ firstName : String, _ lastName : String){
    print(firstName + " " + lastName)
}

printFullName("Pasha", "Polhovskiy")

/*:
 Task 6
 Define a function with two input parameters: 'name' and 'surname' and returning tuplr (Stirng, String) with name and surname
 */

//Answer

//func printTuple (name : String, surname : String) -> (String,String){
//    return (name, surname)
//}
//
//printTuple(name: "Pasha", surname: "Polhovskiy")

/*:
 Taks 7
 Define a function with input parameter: [String?] (array). Function should return an array of type [String]
 For example:
 input array: ["a", nil, "b"]
 output array: ["a", "b"]
 */

//Answer

//func removeNil (array : [String?]) -> [String]{
//    var newArray = [String]()
//    for valueOfArray in array where valueOfArray != nil {
//       newArray.append(valueOfArray!)
//    }
//    return newArray
//}
//
//removeNil(array: ["a", nil, "b", "c", nil])

