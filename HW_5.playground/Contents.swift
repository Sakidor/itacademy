import UIKit

struct Car {
    
    var mark : String
    var countOfdoors : Int
    var bodeStyle : String
    var color : String
    var maxSpeed : Int
    
    var informationOfCar : String {
        return "\(mark) that has \(countOfdoors) doors, \(bodeStyle) body, \(color) color and maxSpeed \(maxSpeed)"
    }
    func moveForward() {
        print("car is going forward")
    }
    
    func description() {
        print(informationOfCar)
    }
    
}

let tesla = Car(mark: "Tesla", countOfdoors: 4 , bodeStyle: "Sedan", color: "Yellow", maxSpeed: 120 )
tesla.description()


struct CarOwner {
    
    var car : Car
    var firstName : String
    var lastName : String
    var categoryOfDrivingLicense : String
    var fullName : String {
        return firstName + " " + lastName
    }
    
    func describeCar() {
        print("full name of car owner \(fullName) which has \(categoryOfDrivingLicense) type of driving licence; Description of his car: \(car.informationOfCar)")
    }
    
}

let person = CarOwner(car: tesla, firstName: "Pavel", lastName: "Polhovskiy", categoryOfDrivingLicense: "B")
person.describeCar()
