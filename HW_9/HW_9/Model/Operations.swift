//
//  Operations.swift
//  HW_9
//
//  Created by Pavel Polhovskiy on 1/8/19.
//  Copyright © 2019 Pavel Polhovskiy. All rights reserved.
//

import Foundation

class Operations {

    static var sharedData = Operations()
    
    private init() {}
    
    func addition (firstNumber: Double, secondNumber: Double) -> Double {
        return firstNumber + secondNumber
    }
    
    func subtraction (firstNumber: Double, secondNumber: Double) -> Double {
        return firstNumber - secondNumber
    }
    
    func multiplication (firstNumber: Double, secondNumber: Double) -> Double {
        return firstNumber * secondNumber
    }
    
    func division (firstNumber: Double, secondNumber: Double) -> Double {
        return firstNumber / secondNumber
    }

}
