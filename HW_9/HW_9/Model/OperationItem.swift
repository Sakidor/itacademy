//
//  OperationItem.swift
//  HW_9
//
//  Created by Pavel Polhovskiy on 1/13/19.
//  Copyright © 2019 Pavel Polhovskiy. All rights reserved.
//

import Foundation


class OperationNumber {

	var firstNumber : Double?
	var secondNumber : Double?
	var operationType: OperationType


	func canCalculate() -> Bool {
		return false
	}

	init(firstNumber : Double, secondNumber : Double, operationType : OperationType) {
		self.firstNumber = firstNumber
		self.secondNumber = secondNumber
		self.operationType = operationType
	}


}
