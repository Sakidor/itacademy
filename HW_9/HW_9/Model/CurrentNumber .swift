//
//  CurrentNumber .swift
//  HW_9
//
//  Created by Pavel Polhovskiy on 1/13/19.
//  Copyright © 2019 Pavel Polhovskiy. All rights reserved.
//

import Foundation

enum CurrentNumber {
	case first
	case second
}
