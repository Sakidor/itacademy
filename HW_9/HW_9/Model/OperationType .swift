//
//  OperationType .swift
//  HW_9
//
//  Created by Pavel Polhovskiy on 1/13/19.
//  Copyright © 2019 Pavel Polhovskiy. All rights reserved.
//

import Foundation


enum OperationType : Int {
	case addition = 1
	case subtraction = 2
	case multiplication = 3
	case division = 4
}







