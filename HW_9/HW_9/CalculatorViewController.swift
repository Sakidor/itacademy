//
//  ViewController.swift
//  HW_9
//
//  Created by Pavel Polhovskiy on 1/8/19.
//  Copyright © 2019 Pavel Polhovskiy. All rights reserved.
//

import UIKit

class CalculatorViewController: UIViewController {
    
	@IBOutlet var numbersFromZeroToNineButtons: [UIButton]!
    @IBOutlet weak var numberOnTheScreenLabel: UILabel!

    var firstNumber: Double?
    var secondNumber: Double?
	var result: Double?

	var tag: Int?

	var currentNumber: CurrentNumber = .first

    @IBAction func tapNumberButton(_ sender: UIButton) {
        if numberOnTheScreenLabel.text == "0" {
            numberOnTheScreenLabel.text?.removeAll()
        }
        if numberOnTheScreenLabel.text!.count < 9 {
        	if let number = numbersFromZeroToNineButtons.index(of: sender) {
            	numberOnTheScreenLabel.text?.append(String(number))
        } else {
            	if !numberOnTheScreenLabel.text!.contains(".") {
                numberOnTheScreenLabel.text?.append(".")
            	}
          	}
		if currentNumber == .first {
			firstNumber = Double(numberOnTheScreenLabel.text!)
			print("first number is \(firstNumber!)")
		} else {
			secondNumber = Double(numberOnTheScreenLabel.text!)
			print("second number is \(secondNumber!)")
			}
		}
	}

	@IBAction func tapOperationsButton(_ sender: UIButton) {
		currentNumber = .second
		numberOnTheScreenLabel.text?.removeAll()
		tag = sender.tag
	}

	@IBAction func tapEqualButton(_ sender: UIButton) {

		guard let firstNumber = firstNumber else {
			return
		}
		guard let secondNumber = secondNumber else {
			return
		}
		if let tag = tag {
			calculateExpression(firstNumber: firstNumber, secondNumber: secondNumber, tag: tag)
		}

		if let result = result {
			numberOnTheScreenLabel.text = String(result)
			self.firstNumber = result
			print("first number now is \(firstNumber)")
			self.secondNumber = nil
			self.result = nil
		}
	}

     @IBAction func deleteNumberButton(_ sender: UIButton) {
          numberOnTheScreenLabel.text = "0"
		  firstNumber = nil
		  secondNumber = nil
		  result = nil
		  currentNumber = .first
	}

	 override func viewDidLoad() {
        super.viewDidLoad()
     }

	 func calculateExpression(firstNumber: Double, secondNumber: Double, tag: Int) {
		switch tag {
			case 1:
				result = Operations.sharedData.addition(firstNumber: firstNumber, secondNumber: secondNumber)
			case 2:
				result = Operations.sharedData.subtraction(firstNumber: firstNumber, secondNumber: secondNumber)
			case 3:
				result = Operations.sharedData.multiplication(firstNumber: firstNumber, secondNumber: secondNumber)
			case 4:
				result = Operations.sharedData.division(firstNumber: firstNumber, secondNumber: secondNumber)
			default:
				break
			}
		}
	}
