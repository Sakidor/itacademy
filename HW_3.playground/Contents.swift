import UIKit

/*
 Task 1
 
 Create a variable named counter and set it equal to 0. Create a while loop with the condition counter < 10 which prints out counter is X (where X is replaced with counter value) and then increments counter by 1.
 */

//Answer:

var counter = 0
while counter < 10 {
    print(counter)
    counter+=1
}



/*
 Task 2
 
 Create a variable named counter and set it equal to 0. Create another variable named roll and set it equal to 0. Create a repeat-while loop. Inside the loop, set roll equal to Int.random(in: 0...5) which means to pick a random number between 0 and 5. Then increment counter by 1. Finally, print After X rolls, roll is Y where X is the value of counter and Y is the value of roll. Set the loop condition such that the loop finishes when the first 0 is rolled.
 */

//Answer

var counter2 = 0
var roll = 0
repeat{
    roll = Int.random(in: 0...5)
    counter2+=1
    print("Random roll is \(roll) for \(counter) time")
}while roll != 0


/*
 Task 3
 
 Create a constant named range, and set it equal to a range starting at 1 and ending with 10 inclusive.
 Write a for loop that iterates over this range and prints the square of each number.
 */

//Answer

let range = 1...10
for index in range {
    let result = pow(Decimal(index),2)
    print(result)
}

/*
 Task 4
 
 Below you can see an example of for loop that iterates over only the even rows like so:
 */

var sum = 0
for row in 0..<8 {
    if row % 2 == 0 {
        continue
    }

    for column in 0..<8 {
        sum += row * column
    }
}
/*
 Change this to use a where clause on the first for loop to skip even rows instead of using continue. Check that the sum is 448 as in the initial example.
 */

//Answer

var sum2 = 0
for row in 0..<8 where row % 2 != 0{
    for column in 0..<8 {
        sum2 += row * column
    }
    print(sum2)
}



/*
 Task 5
 
 Print a table of the first 10 powers of 2
 */

var count = 0
for i in 0...9{
    count += 1
    let result = pow(2,i)
    let str = ("""
        | the \(count) power of 2 is \(result)|
        ---------------------------
        """)
    print(str)
}



/*
 Task 6
 
 Given a number n, calculate the factorial of n.
 Example: 4 factorial is equal to 1 * 2 * 3 * 4
 */

//Answer

func calculateFactorial (number : Int){
    var result = 1
    for index in 1..<number{
    result *= index+1
    }
    result
}

calculateFactorial(number: 5)


/*
 Task 7
 
 Given a number n, calculate the n-th Fibonacci number. (Recall Fibonacci is 1, 1, 2, 3, 5, 8, 13, ... Start with 1 and 1 and add these values together to get the next value. The next value is the sum of the previous two. So the next value in this case is 8+13 = 21.)
 */

//Answer

func calculateFibonacci(number : Int) -> Int {
    if (number == 0) {
        return 0
    }
    if (number == 1) {
        return 1
        }
    return calculateFibonacci(number: number - 2) + calculateFibonacci(number: number - 1)
}

calculateFibonacci(number: 8)

/*
 Task 8
 
 Write a switch statement that takes an age as an integer and prints out the life stage related to that age. You can make up the life stages, or use my categorization as follows: 0-2 years, Infant; 3-12 years, Child; 13-19 years, Teenager; 20-39, Adult; 40-60, Middle aged; 61+, Elderly.
 */

//Answer
func calculateLifeStage (age : Int){
    switch age {
    case 0...2:
        print("Infant")
    case 3...12:
        print("Child")
    case 13...19:
        print("Teenager")
    case 40...60:
        print("Middle aged")
    case 61...:
        print("Elderly")
    default:
        print("please input corect age")
    }
}

calculateLifeStage(age: -6)



/*
 Task 9
 
 Write a switch statement that takes a tuple containing a string and an integer. The string is a name, and the integer is an age. Use the same cases that you used in the previous exercise and let syntax to print out the name followed by the life stage. For example, for myself it would print out "Slava is an adult."
 */

//Answer


let tuple = (name : "Pavel", age : 19 )

switch tuple {
    case (_ ,0...2):
        print("\(tuple.name) is Infant")
    case (_, 3...12):
        print("\(tuple.name) is Child")
    case (_,13...19):
        print("\(tuple.name) is Teenager")
    case (_,40...60):
        print("\(tuple.name) is Middle aged")
    case (_,61...):
        print("\(tuple.name) is Elderly")
    default:
        print("please input corect age")
    }




