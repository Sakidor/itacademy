import UIKit

/*
 
 Task 1
 
 “Declare two constants a and b of type Double and assign both a value. Calculate the average of a and b and store the result in a constant named average.”
 */
/*
 Task 2
 
 “A temperature expressed in °C can be converted to °F by multiplying by 1.8 then incrementing by 32. In this challenge, do the raeverse: convert a temperature from °F to °C. Declare a constant named fahrenheit of type Double and assign it a value. Calculate the corresponding temperature in °C and store the result in a constant named celcius.”
 */
/*
 Task 3
 
 “Create a string constant called firstName and initialize it to your first name. Also create a string constant called lastName and initialize it to your last name. Create a string constant called fullName by adding the firstName and lastName constants together, separated by a space.
 Using interpolation, create a string constant called myDetails that uses the fullName constant to create a string introducing yourself. For example, my string would read: "Hello, my name is {here enter your name}}.”
 */


//Task 1
let a : Double = 3.12
let b : Double = 6.88

let average = (a+b)/2

//Task 2

let fahrenheit : Double = 60
let celcius = (fahrenheit - 32)/1.8

// Task 3
let firstName = "Pavel"
let lastName = "Polhovskiy"

let fullname = firstName + " " + lastName
let myDetails = "Hi! My name is \(fullname)"


