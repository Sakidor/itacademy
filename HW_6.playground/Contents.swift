import UIKit


struct Point {
    
    var x : Int
    var y : Int
    
    init(x : Int, y : Int) {
        self.x = x
        self.y = y
    }
    
}
// Points for first player 
var pointForShipWithOneDeckOfFirstPlayer1 = Point(x: 10, y: 1)
var pointForShipWithOneDeckOfFirstPlayer2 = Point(x: 9, y: 4)
var pointForShipWithOneDeckOfFirstPlayer3 = Point(x: 9, y: 6)
var pointForShipWithOneDeckOfFirstPlayer4 = Point(x: 6, y: 7)

var firstPointForShipWithTwoDecksOfFirstPlayer1 = Point(x: 1, y: 1)
var secondPointForShipWithTwoDecksOfFirstPlayer1 = Point(x: 1, y: 2)

var firstPointForShipWithTwoDecksOfFirstPlayer2 = Point(x: 9, y: 9)
var secondPointForShipWithTwoDecksOfFirstPlayer2 = Point(x: 10, y: 9)

var firstPointForShipWithTwoDecksOfFirstPlayer3 = Point(x: 3, y: 10)
var secondPointForShipWithTwoDecksOfFirstPlayer3 = Point(x: 4, y: 10)

var firstPointForShipWithThreeDecksOfFirstPlayer1 = Point(x: 5, y: 2)
var secondPointForShipWithThreeDecksOfFirstPlayer1 = Point(x: 6, y: 2)
var thirdPointForShipWithThreeDecksOfFirstPlayer1 = Point(x: 7, y: 2)

var firstPointForShipWithThreeDecksOfFirstPlayer2 = Point(x: 1, y: 4)
var secondPointForShipWithThreeDecksOfFirstPlayer2 = Point(x: 2, y: 4)
var thirdPointForShipWithThreeDecksOfFirstPlayer2 = Point(x: 3, y: 4)

var firstPointForShipWithFourDecksOfFirstPlayer = Point(x: 1, y: 7)
var secondPointForShipWithFourDecksOfFirstPlayer = Point(x: 1, y: 8)
var thirdPointForShipWithFourDecksOfFirstPlayer = Point(x: 1, y: 9)
var fourPointForShipWithFourDecksOfFirstPlayer = Point(x: 1, y: 10)

// Points for second player
var pointForShipWithOneDeckOfSecondPlayer1 = Point(x: 10, y: 1)
var pointForShipWithOneDeckOfSecondPlayer2 = Point(x: 9, y: 4)
var pointForShipWithOneDeckOfSecondPlayer3 = Point(x: 9, y: 6)
var pointForShipWithOneDeckOfSecondPlayer4 = Point(x: 6, y: 7)

var firstPointForShipWithTwoDecksOfSecondPlayer1 = Point(x: 1, y: 1)
var secondPointForShipWithTwoDecksOfSecondPlayer1 = Point(x: 1, y: 2)

var firstPointForShipWithTwoDecksOfSecondPlayer2 = Point(x: 9, y: 9)
var secondPointForShipWithTwoDecksOfSecondPlayer2 = Point(x: 10, y: 9)

var firstPointForShipWithTwoDecksOfSecondPlayer3 = Point(x: 3, y: 10)
var secondPointForShipWithTwoDecksOfSecondPlayer3 = Point(x: 4, y: 10)

var firstPointForShipWithThreeDecksOfSecondPlayer1 = Point(x: 5, y: 2)
var secondPointForShipWithThreeDecksOfSecondPlayer1 = Point(x: 6, y: 2)
var thirdPointForShipWithThreeDecksOfSecondPlayer1 = Point(x: 7, y: 2)

var firstPointForShipWithThreeDecksOfSecondPlayer2 = Point(x: 1, y: 4)
var secondPointForShipWithThreeDecksOfSecondPlayer2 = Point(x: 2, y: 4)
var thirdPointForShipWithThreeDecksOfSecondPlayer2 = Point(x: 3, y: 4)

var firstPointForShipWithFourDecksOfSecondPlayer = Point(x: 1, y: 7)
var secondPointForShipWithFourDecksOfSecondPlayer = Point(x: 1, y: 8)
var thirdPointForShipWithFourDecksOfSecondPlayer = Point(x: 1, y: 9)
var fourPointForShipWithFourDecksOfSecondPlayer = Point(x: 1, y: 10)


extension Point : Equatable {
    
    static func == (lhs: Point, rhs: Point) -> Bool {
        return lhs.x == rhs.x && lhs.y == rhs.y
    }
}


class Ship {
    
    var coordinates = [Point]()
    
    init(coordinates: [Point]){
        self.coordinates = coordinates
    }
}

// Ships for first player
var shipWithOneDeckForFirstPlayer1 = Ship(coordinates: [pointForShipWithOneDeckOfFirstPlayer1])
var shipWithOneDeckForFirstPlayer2 = Ship(coordinates: [pointForShipWithOneDeckOfFirstPlayer2])
var shipWithOneDeckForFirstPlayer3 = Ship(coordinates: [pointForShipWithOneDeckOfFirstPlayer3])
var shipWithOneDeckForFirstPlayer4 = Ship(coordinates: [pointForShipWithOneDeckOfFirstPlayer4])
var shipWithTwoDeckForFirstPlayer1 = Ship(coordinates: [firstPointForShipWithTwoDecksOfFirstPlayer1, secondPointForShipWithTwoDecksOfFirstPlayer1 ])
var shipWithTwoDeckForFirstPlayer2 = Ship(coordinates: [firstPointForShipWithTwoDecksOfFirstPlayer2, secondPointForShipWithTwoDecksOfFirstPlayer2])
var shipWithTwoDeckForFirstPlayer3 = Ship(coordinates: [firstPointForShipWithTwoDecksOfFirstPlayer3, secondPointForShipWithTwoDecksOfFirstPlayer3])
var shipWithThreeDeckForFirstPlayer1 = Ship(coordinates: [firstPointForShipWithThreeDecksOfFirstPlayer1, secondPointForShipWithThreeDecksOfFirstPlayer1, thirdPointForShipWithThreeDecksOfFirstPlayer1])
var shipWithThreeDeckForFirstPlayer2 = Ship(coordinates: [firstPointForShipWithThreeDecksOfFirstPlayer2, secondPointForShipWithThreeDecksOfFirstPlayer2, thirdPointForShipWithThreeDecksOfFirstPlayer2])
var shipWithFourDeckForFirstPlayer = Ship(coordinates: [firstPointForShipWithFourDecksOfFirstPlayer, secondPointForShipWithFourDecksOfFirstPlayer, thirdPointForShipWithFourDecksOfFirstPlayer,fourPointForShipWithFourDecksOfFirstPlayer])


// Ships for second player
var shipWithOneDeckForSecondPlayer1 = Ship(coordinates: [pointForShipWithOneDeckOfSecondPlayer1])
var shipWithOneDeckForSecondPlayer2 = Ship(coordinates: [pointForShipWithOneDeckOfSecondPlayer2])
var shipWithOneDeckForSecondPlayer3 = Ship(coordinates: [pointForShipWithOneDeckOfSecondPlayer3])
var shipWithOneDeckForSecondPlayer4 = Ship(coordinates: [pointForShipWithOneDeckOfSecondPlayer4])
var shipWithTwoDeckForSecondPlayer1 = Ship(coordinates: [firstPointForShipWithTwoDecksOfSecondPlayer1, secondPointForShipWithTwoDecksOfSecondPlayer1])
var shipWithTwoDeckForSecondPlayer2 = Ship(coordinates: [firstPointForShipWithTwoDecksOfSecondPlayer2, secondPointForShipWithTwoDecksOfSecondPlayer2])
var shipWithTwoDeckForSecondPlayer3 = Ship(coordinates: [firstPointForShipWithTwoDecksOfSecondPlayer3, secondPointForShipWithTwoDecksOfSecondPlayer3])
var shipWithThreeDeckForSecondPlayer1 = Ship(coordinates: [firstPointForShipWithThreeDecksOfSecondPlayer1, secondPointForShipWithThreeDecksOfSecondPlayer1, thirdPointForShipWithThreeDecksOfSecondPlayer1])
var shipWithThreeDeckForSecondPlayer2 = Ship(coordinates: [firstPointForShipWithThreeDecksOfSecondPlayer2, secondPointForShipWithThreeDecksOfSecondPlayer2, thirdPointForShipWithThreeDecksOfSecondPlayer2])
var shipWithFourDeckForSecondPlayer = Ship(coordinates: [firstPointForShipWithFourDecksOfSecondPlayer, secondPointForShipWithFourDecksOfSecondPlayer, thirdPointForShipWithFourDecksOfSecondPlayer,fourPointForShipWithFourDecksOfSecondPlayer])



class Player {
    
    var name : String
    var arrayOfShips = [Ship]()
    

    init(name : String, arrayOfShips : [Ship]) {
        self.name = name
        self.arrayOfShips = arrayOfShips
    }
}

var playerOne = Player(name: "Vlad", arrayOfShips: [shipWithOneDeckForFirstPlayer1, shipWithOneDeckForFirstPlayer2, shipWithOneDeckForFirstPlayer3, shipWithOneDeckForFirstPlayer4, shipWithTwoDeckForFirstPlayer1, shipWithTwoDeckForFirstPlayer2, shipWithTwoDeckForFirstPlayer3, shipWithThreeDeckForFirstPlayer1, shipWithThreeDeckForFirstPlayer2, shipWithFourDeckForFirstPlayer])

var playerTwo = Player(name: "Pavel", arrayOfShips: [shipWithOneDeckForSecondPlayer1, shipWithOneDeckForSecondPlayer2, shipWithOneDeckForSecondPlayer3, shipWithOneDeckForSecondPlayer4, shipWithTwoDeckForSecondPlayer1, shipWithTwoDeckForSecondPlayer2, shipWithTwoDeckForSecondPlayer3, shipWithThreeDeckForSecondPlayer1, shipWithThreeDeckForSecondPlayer2, shipWithFourDeckForSecondPlayer])


class Game {
    
  static func hit(player : Player, coordinateOfShoot : Point) -> Bool {
    for ship in player.arrayOfShips {
        for point in ship.coordinates {
            if (coordinateOfShoot == point) {
                return true
            }
        }
    }
    return false
 }
}
var pointOfShootForFirstPlayer = Point(x: 5, y: 2)
var pointOfShootForSecondPlayer = Point(x: 5, y: 5)

Game.hit(player: playerOne, coordinateOfShoot: pointOfShootForSecondPlayer)
Game.hit(player: playerTwo, coordinateOfShoot: pointOfShootForFirstPlayer)
