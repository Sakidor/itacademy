//
//  InstaPostCell.swift
//  HW_8
//
//  Created by Pavel Polhovskiy on 12/25/18.
//  Copyright © 2018 Pavel Polhovskiy. All rights reserved.
//

import UIKit

class InstaPostCell: UITableViewCell {

	// MARK: - Outlets

    @IBOutlet weak var acсountLogoImageView: UIImageView!
    @IBOutlet weak var photoOfUserImageView: UIImageView!
    @IBOutlet weak var nickNameOfUserLabel: UILabel!
    @IBOutlet weak var locationOfUserLabel: UILabel!
    
	// MARK: - Public

    func update (item : PostItem) {
        acсountLogoImageView.image = item.accountLogo
        photoOfUserImageView.image = item.photo
        nickNameOfUserLabel.text = item.accountName
        locationOfUserLabel.text = item.location
    }

	// MARK: - Lifecycle

    override func awakeFromNib() {
        super.awakeFromNib()
        acсountLogoImageView.roundCorners()
    }
}



