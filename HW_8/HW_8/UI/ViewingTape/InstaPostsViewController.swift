//
//  InstaPostsViewController.swift
//  HW_8
//
//  Created by Pavel Polhovskiy on 12/25/18.
//  Copyright © 2018 Pavel Polhovskiy. All rights reserved.
//

import UIKit

class InstaPostsViewController: UIViewController {
    
    // MARK: - Properties
    
    var postItems : [PostItem] = []

    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupData()
    }
    
    // MARK: - Data
    
    func setupData() {
        let instaItem1 = PostItem(accountLogo: UIImage(named: "olya1"), photo: UIImage(named: "olya1"), accountName: "logvin_olga", location: "Minsk")
        let instaItem2 = PostItem(accountLogo: UIImage(named: "olya1") , photo: UIImage(named: "olya2"), accountName: "logvin_olga", location: "Baranovichi")
        let instaItem3 = PostItem(accountLogo: UIImage(named: "pavel1"), photo: UIImage(named: "pavel2"), accountName: "polhovskiy_", location: "Pinsk")
        let instaItem4 = PostItem(accountLogo: UIImage(named: "oxxxy1"), photo: UIImage(named: "oxxxy1"), accountName: "norimyxxxo", location: "Тверь Россия")
        let instaItem5 = PostItem(accountLogo: UIImage(named: "vova1"), photo: UIImage(named: "vova2"), accountName: "kudelich_", location: "New York")
        let instaItem6 = PostItem(accountLogo: UIImage(named: "valera1"), photo: UIImage(named: "valera1"), accountName: "valerakazak", location: "Pinsk")
        let instaItem7 = PostItem(accountLogo: UIImage(named: "olya1"), photo: UIImage(named: "olya3"), accountName: "logvin_olga", location: nil)
        let instaItem8 = PostItem(accountLogo: UIImage(named: "flow1"), photo: UIImage(named: "flow2"), accountName: "theflow.ru", location: nil)
        let instaItem9 = PostItem(accountLogo: UIImage(named: "pavel1"), photo: UIImage(named: "pavel2"), accountName: "polhovskiy_", location: "Minsk")
        let instaItem10 = PostItem(accountLogo: UIImage(named: "pavel1"), photo: UIImage(named: "pavel3" ), accountName: "polhovskiy_", location: "Baranovichi")
        
        postItems = [instaItem1, instaItem2, instaItem3, instaItem4, instaItem5, instaItem6, instaItem7, instaItem8, instaItem9, instaItem10]
    }
}

// MARK: - UITableViewDataSource extension

extension InstaPostsViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return postItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let postCell = tableView.dequeueReusableCell(withIdentifier: "cellOfInstagram", for: indexPath) as? InstaPostCell {
            let instaItem = postItems[indexPath.row]
            postCell.update(item: instaItem)

			return postCell
		} else {
			fatalError("Invalid cell at indexPath: \(indexPath)")
		}
	}

}




