//
//  UIImageUtils.swift
//  HW_8
//
//  Created by Pavel Polhovskiy on 1/4/19.
//  Copyright © 2019 Pavel Polhovskiy. All rights reserved.
//

import UIKit

// MARK: - UIImageView extension

extension UIImageView {
    
    func roundCorners() {
        self.layer.cornerRadius = (self.frame.width / 2)
        self.layer.masksToBounds = true
    }
}
