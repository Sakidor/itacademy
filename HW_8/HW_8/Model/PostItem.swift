//
//  InformationOfUser.swift
//  HW_8
//
//  Created by Pavel Polhovskiy on 1/4/19.
//  Copyright © 2019 Pavel Polhovskiy. All rights reserved.
//

import UIKit


class PostItem {
    
    // MARK: - Properties
    
    var accountLogo: UIImage?
    var photo: UIImage?
    var accountName: String
    var location: String?

	// MARK: - Lifecycle
    
    init(accountLogo: UIImage?, photo: UIImage?, accountName: String, location: String?) {
        self.accountLogo = accountLogo
        self.photo = photo
        self.accountName = accountName
        self.location = location
    }
    
}
